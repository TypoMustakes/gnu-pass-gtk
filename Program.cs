using System;
using Gtk;

namespace gnu_pass_gtk
{
    class Program
	{
		[STAThread]
		public static void Main(string[] args)
		{
			Application.Init();

            //var app = new Application("org.gnu_pass_gtk.gnu_pass_gtk", GLib.ApplicationFlags.None);
            //app.Register(GLib.Cancellable.Current);

            //var win = new MainWindow();
            //app.AddWindow(win);

            //win.Show();

            //            Window myWin = new Window("My first GTK# Application! ");
            //            myWin.Resize(200, 200);
            //
            //            //Create a label and put some text in it.
            //            Label myLabel = new Label();
            //            myLabel.Text = "Hello World!!!!";
            //
            //            VBox myListbox = new VBox(true, 3);
            //            myListbox.Add(myLabel);
            //            myListbox.Add(new Button("hehe"));
            //            myListbox.Add(new Button("hehe"));
            //            myListbox.Add(new Button("hehe"));
            //
            //            myWin.Add(myListbox);
            //
            //            //Add the label to the form
            //            //myWin.Add(myLabel);
            //
            //            //Show Everything
            //            myWin.ShowAll();

            MainWindow main = new MainWindow();
            main.ShowAll();

            Application.Run();

		}
	}
}
