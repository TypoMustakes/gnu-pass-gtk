using System;
using Gtk;

namespace gnu_pass_gtk
{
    sealed partial class MainWindow : Window
    {
        //public MainWindow() : this(new Builder("MainWindow.glade")) { }

		// private MainWindow(Builder builder) : base(builder.GetRawOwnedObject("MainWindow"))
		// {
		//     builder.Autoconnect(this);

		//     DeleteEvent += Window_DeleteEvent;
		//     _button1.Clicked += Button1_Clicked;
		// }


        private void SearchQueryChangedEvent(object sender, EventArgs e)
        {
            results.SearchQuery = searchBox.Text;
            results.Refill(GetPasswords());
        }

        private string[] GetPasswords()
        {
            ProcessBuilder pb = new ProcessBuilder();
            pb.ProcessFailed += (Exception e) => { ErrorMessage(e); };

            string? output = pb.GetOutput("./getpwds.sh", "");
            if (output != null)
            {
                return output.Split('\n');
            }

            return new string[1];
        }

        private void ErrorMessage(Exception e)
        {
            Window errorWindow = new Window(Gtk.WindowType.Toplevel);
            errorWindow.Title = "Error";

            VBox c1 = new VBox();
            c1.Homogeneous = true;
            errorWindow.Add(c1);

            HBox c2 = new HBox();
            c2.Homogeneous = false;
            Label tmp = new Label(e.Message);
            tmp.Wrap = true;
            tmp.SetPadding(100, 10);
			
            c2.Add(tmp);
            c1.Add(c2);

            Button btn = new Button();
            btn.Label = "OK";
            btn.Clicked += (sender, e) => { errorWindow.Destroy(); };
            c1.Add(btn);

            errorWindow.Add(new Label(e.Message));
            errorWindow.ShowAll();
        }

        private void Window_DeleteEvent(object sender, DeleteEventArgs a)
        {
            Application.Quit();
        }

		// private void Button1_Clicked(object sender, EventArgs a)
		// {
		//     _counter++;
		//     _label1.Text = "Hello World! This button has been clicked " + _counter + " time(s).";
		// }
    }
}
