using System;
using Gtk;
using System.Collections.Generic;

namespace gnu_pass_gtk
{
    class ListBox : VBox
    {
        private const uint ITEM_PADDING = 5;
        private const bool ITEM_EXPAND = false;
        private const bool ITEM_FILL = false;

        /// <summary>
        /// Should contain the string to display results for.
        /// If <c>SearchQuery</c> is not null, then the list will
        /// filter all <c>Label</c> children and delete those that
        /// do not contain it as a substring.
        /// </summary>
        public string? SearchQuery { get; set; }

        public ListBox() : base()
        {
            SearchQuery = null;
        }

        public ListBox(string[] items) : this()
        {
            Load(items);
        }

        private void Clear()
        {
            foreach (Widget w in this.Children)
            {
                this.Remove(w);
            }
        }

        private void Load(string[] arr)
        {
            Clear();
            Array.Sort(arr);
            foreach (string s in arr)
            {
                if (s != "")
                {
					// If you want results to be on the left

					// HBox hbox = new HBox();
					// Label label = new Label(s);
					// label.Name = s;
					// label.LabelProp = s;
					// hbox.PackStart(label, false, false, 5);
					// hbox.PackStart(new Label(), true, false, 0);
					// this.PackStart(hbox, ITEM_EXPAND, ITEM_FILL, ITEM_PADDING);
					// this.PackStart(new VSeparator(), false, false, 0);

					// If you want results to be centered

					Label label = new Label(s);
					label.Name = s;
					label.LabelProp = s;
					this.PackStart(label, ITEM_EXPAND, ITEM_FILL, ITEM_PADDING);
					this.PackStart(new VSeparator(), false, false, 0);
                }
            }
        }

		/// <summary>
		/// All current children are deleted, and the VBox will be filled with
		/// Label objects, with each of their <c>Text</c> properties set to each
		/// string element in the array.
		/// If a search query is set, all Label children will be filtered against it.
		/// </summary>
		/// <param name="method">
		/// Returns <c>string[]</c>, with each item representing a <c>Label</c>'s text.
		/// </param>
        public void Refill(string[] items)
        {
			this.Load(items);
			List<string> validLabels = new List<string>();
			
			if (SearchQuery != null)
			{
				foreach (Widget w in this.Children)
				{
					if (w is Label)
					{
						Label tmp = (Label)w;
						if (tmp.Text.Contains(SearchQuery))
						{
							validLabels.Add(tmp.Text);
						}
					}
				}

                this.Clear();
                this.Load(validLabels.ToArray());
            }
            this.ShowAll();
        }
    }
}
