using Gtk;

namespace gnu_pass_gtk
{
    public partial class MainWindow
    {
        private VBox topContainer;
        private Entry searchBox;
        private ScrolledWindow GtkScrolledWindow;
        private ListBox results;
        private Button decryptBtn;

        public MainWindow() : base(WindowType.Toplevel)
        {
			#region Window properties
            this.Name = "MainWindow";
            this.Title = "Password Manager";
            this.Resizable = true;
            this.WindowPosition = ((WindowPosition)(4));
			#endregion

			#region Container properties
            this.topContainer = new VBox();
            this.topContainer.Name = "topContainer";
            this.topContainer.Spacing = 6;
			#endregion

			#region SearchBox 
            this.searchBox = new Entry();
            this.searchBox.CanFocus = true;
            this.searchBox.Name = "searchBox";
            this.searchBox.IsEditable = true;
            this.searchBox.InvisibleChar = '•';
            this.searchBox.MarginLeft = 5;
            this.searchBox.MarginRight = 5;
            this.searchBox.Changed += SearchQueryChangedEvent;
            this.topContainer.Add(this.searchBox);
            Box.BoxChild w1 = (Box.BoxChild)(this.topContainer[this.searchBox]);
            w1.Position = 0;
            w1.Expand = false;
            w1.Fill = false;
			#endregion

			#region GtkScrolledWindow properties
            this.GtkScrolledWindow = new ScrolledWindow();
            this.GtkScrolledWindow.Name = "GtkScrolledWindow";
            this.GtkScrolledWindow.ShadowType = ((ShadowType)(1));
			#endregion

			#region GtkScrolledWindow.Viewport
            Viewport viewPort = new Viewport();
            viewPort.ShadowType = ((ShadowType)(0));
			#endregion

			#region Results properties
            this.results = new ListBox(GetPasswords());
            this.results.Name = "results";
            this.results.Spacing = 6;
            viewPort.Add(this.results);
            #endregion

            GtkScrolledWindow.Add(viewPort);
            topContainer.Add(GtkScrolledWindow);

            #region Button properties
            this.decryptBtn = new Button();
            this.decryptBtn.CanFocus = true;
            this.decryptBtn.Name = "decryptBtn";
            this.decryptBtn.UseUnderline = true;
            this.decryptBtn.Label = "Decrypt";
            this.topContainer.Add(this.decryptBtn);
            Box.BoxChild w2 = ((Box.BoxChild)(this.topContainer[this.decryptBtn]));
            w2.Position = 2;
            w2.Expand = false;
            w2.Fill = false;
            #endregion
			
            this.Add(this.topContainer);

            if ((this.Child != null))
            {
                this.Child.ShowAll();
            }
            this.DefaultWidth = 300;
            this.DefaultHeight = 450;
            this.Show();
            //this.DeleteEvent += new DeleteEventHandler(this.OnDeleteEvent);
        }
    }
}
